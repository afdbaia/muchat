## Contributing to Muchat

### Reporting issue

### Code Conventions

Our code style is almost in line with the standard java conventions (Popular IDE's default setting satisfy this), with
the following additional restricts:

* If there are more than 120 characters in the current line, begin a new line.

* Make sure all new .java files to have a simple Javadoc class comment with at least a @date tag identifying birth, and
  preferably at least a paragraph on the intended purpose of the class.

* Add the ASF license header comment to all new .java files (copy from existing files in the project)

* Make sure no @author tag gets appended to the file you contribute to as the @author tag is incompatible with Apache.
  Rest assured, other ways, including CVS, will ensure transparency, fairness in recording your contributions.

* Add some Javadocs and, if you change the namespace, some XSD doc elements.

* Sufficient unit-tests should accompany new feature development or non-trivial bug fixes.

* If no-one else is using your branch, please rebase it against the current master (or another target branch in the main
  project).

* When writing a commit message, please follow the following conventions: should your commit address an open issue,
  please add Fixes #XXX at the end of the commit message (where XXX is the issue number).

### Code style

We provide a template file [codestyle_for_idea.xml](./codestyle_for_idea.xml) for IntelliJ idea that you can import it
to your workplace.
If you use Eclipse, you can use the IntelliJ Idea template for manually configuring your file.

**NOTICE**

It's critical to set the codestyle_for_idea.xml to avoid the failure of your Travis CI builds. Steps to configure the
code styles are as follows:

1. Enter `Editor > Code Style`
2. To manage a code style scheme, in the Code Style page, select the desired scheme from the drop-down list, and click
   on ![manage profiles](codestyle/manage_profiles.png).
   From the drop-down list, select `Import Scheme`, then choose the option `IntelliJ IDEA code style XML` to import the
   scheme.
3. In the Scheme field, type the name of the new scheme and press ⏎ to save the changes.
